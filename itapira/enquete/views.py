from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import Http404
from .models import Questao


# Create your views here.
def index(request):
    ultimas_questoes = Questao.objects.order_by("-data")  # [:4]
    context = {"ultimas_questoes": ultimas_questoes}
    return render(request, "enquete/index.html", context)


def loucura(request):
    return HttpResponse("E nessa loucura.....")


def detalhe(request, questao_id):
    # try:
    #     questao = Questao.objects.get(pk=questao_id)
    # except Questao.DoesNotExist:
    #     raise Http404("Questão non ecxisty")
    # get_object_or_404 tenta fazer um select e retorna automaticamente 404 se não houver elementos
    questao = get_object_or_404(Questao, pk=questao_id)
    return render(request, "enquete/questao.html", {'questao': questao})


def resultados(request, questao_id):
    return HttpResponse("voce esta olhando o resultado da questao %s" % questao_id)


def voto(request, questao_id):
    return HttpResponse("voce esta votando na questao %s" % questao_id)
