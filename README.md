# django-demo

## Comandos
- Criar um Projeto
    - `python -m django startproject <nome>`
- Entrar na pasta
    - `cd itapira`
- Criar um app
    - `python manage.py startapp <nome>`
- Ligar o servidor
    - `python manage.py runserver`
- Criar a lista de dependências
    - `pip freeze > requirements.txt`
- Instalar dependências pela lista
    - `pip install -r requirements.txt`